/*
 * global.h
 *
 *  Created on: 14.09.2011
 *      Author: Venci
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "pins.h"
#include "lcd_config.h"
#include "splc780d1.h"
#include "tc1604a.h"
#include "lcd.h"

#endif /* GLOBAL_H_ */
