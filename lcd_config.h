/*
 * lcd_config.h
 *
 *  Created on: 16.09.2011
 *      Author: Venci
 */

#ifndef LCD_CONFIG_H_
#define LCD_CONFIG_H_

#define LCD_CMD_PORT D

#define LCD_DATA_PORT B

#define LCD_RS PD4
#define LCD_RS_DIR DD4

#define LCD_RW PD5
#define LCD_RW_DIR DD5

#define LCD_E PD6
#define LCD_E_DIR DD6

#endif /* LCD_CONFIG_H_ */
