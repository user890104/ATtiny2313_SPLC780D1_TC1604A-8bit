/*
 * main.c
 *
 *  Created on: 14.09.2011
 *      Author: Venci
 */

#include "global.h"

extern const unsigned char lcd_custom_char[];

int main(void)
{
    lcd_init();

    lcd_home();
/*
    lcd_string_P(PSTR("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"));
    lcd_setcursor(0, 1);
    lcd_string_P(PSTR("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"));
    lcd_setcursor(0, 2);
    lcd_string_P(PSTR("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"));
    lcd_setcursor(0, 3);
    lcd_string_P(PSTR("\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"));
*/
    //lcd_string_P(PSTR("Hello, world!"));

    while (1) {
        lcd_setcursor(5, 2);
        lcd_string_P(PSTR("      "));
        lcd_setcursor(6, 3);
        lcd_string_P(PSTR("     "));

        for (uint8_t i = 0; i < 8; ++i) {
            lcd_generatechar(i, lcd_custom_char + i * 8);
        }

        lcd_setcursor(8, 0);
        lcd_data(0);
        lcd_string_P(PSTR("\x01"));
        lcd_setcursor(5, 1);
        lcd_string_P(PSTR("\x02\x03\x04\x05\x06\x07"));

        _delay_ms(100);

        lcd_setcursor(8, 0);
        lcd_string_P(PSTR("  "));
        lcd_setcursor(5, 1);
        lcd_string_P(PSTR("      "));

        for (uint8_t i = 0; i < 8; ++i) {
            lcd_generatechar(i, lcd_custom_char + (i + 8) * 8);
        }

        lcd_setcursor(5, 2);
        lcd_data(0);
        lcd_string_P(PSTR("\xff\xff\xff\x01\x02"));
        lcd_setcursor(6, 3);
        lcd_string_P(PSTR("\x03\x04\x05\x06\x07"));

        _delay_ms(100);
    }

    while (1)
        ;

    return 0;
}
